package PageObjects;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import Utility.WaitClass;
import junit.framework.Assert;

public class verifyLinks
{
	@FindBy(how=How.XPATH, using="//img[@alt='New Live Session']/..")
	public static WebElement newSessionLink;
	
	public static void checkingLinks()
	{
		WaitClass.waitfor(newSessionLink);
		String url = "";
		HttpURLConnection huc = null;
		int respcode = 200;
		List<WebElement> list = initBrowser.driver.findElements(By.tagName("a"));
		Iterator<WebElement> it = list.iterator();
		while(it.hasNext())
		{
			url = it.next().getAttribute("href");
			if(url==null || url.isEmpty())
			{
				System.out.println("Link is broken");
				continue;
			}
			try
			{
				huc = (HttpURLConnection)(new URL(url).openConnection());
				huc.setRequestMethod("HEAD");
				huc.connect();
				respcode = huc.getResponseCode();
				if(respcode>=400)
				{
					System.out.println(url+" is broken");
				}
				else
				{
					System.out.println(url+" is fine");
				}
			}
			catch(Exception e)
			{
				
			}
		}
	}
}
