package stepDefinition;

import java.io.IOException;
import org.openqa.selenium.support.PageFactory;
import PageObjects.initBrowser;
import PageObjects.verifyLinks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class stepDefinition 
{	
	@Given("User trigger the browser and enter URL")
	public void user_trigger_the_browser_and_enter_url() throws IOException 
	{
		initBrowser.triggerBrowser();	
	}

	@When("Website will be displayed")
	public void website_will_be_displayed() throws InterruptedException 
	{	
		initBrowser.openURL();
	}
	
	@Then("Verify whether all the links are working or not")
	public void verify_whether_all_the_links_are_working_or_not()
	{
		PageFactory.initElements(initBrowser.driver, verifyLinks.class);
		verifyLinks.checkingLinks();
		initBrowser.driver.quit();
	}
}
